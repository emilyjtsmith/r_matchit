install.packages("Rcpp")
install.packages("MatchIt")

library(bigrquery)
library(tidyverse)
library(MatchIt)

#Loaded table with Items, Trans and Sales for DEC2020. 

df <-  bq_table_download(bq_project_query("aw-insight-dev-live-20", "select * from `aw-insight-dev-live-20.zscratch_emiben.SI_IPICKCTRL_R_01`", stringsAsFactors = FALSE))

#Filter data to pre-period to find a control
df1 <- df %>%
  filter(PRE_PERIOD==1)


#Select the group you want to find a look-a-like audience for.
#In this case I have flipped target and control around as we will
#cutting down the target group (as it contains more people).
df2 <- df1 %>%
  filter(TARGET==1)
df3 <- df1 %>%
  filter(TARGET==0)
#Assign the two data frames to factors called Test$Sample and Control$Sample
Test <- df2
Control <- df3
Test$Sample <- as.factor('Test')
Control$Sample <- as.factor('Control')
#Assigning the chosen group & 0 any nulls.
mydata <- rbind(Test, Control)
mydata$Group <- as.logical(mydata$Sample == 'Test')
mydata[is.na(mydata)] = 0
#Select the variables you want to try and match as well as the method and the ratio of users.
#Ratio has to be >=1
set.seed(1234)
match.it <- matchit(Group ~ TOTAL_SALES + TOTAL_TRANS + TOTAL_ITEMS + DM + MAT + NP + APR_DM, data = mydata, method="nearest", ratio=1)
a <- summary(match.it)
#Pretty good!
#Evaluation functions.
a$nn
a$sum.matched
#Pull the data back out of the match it function
df4 <- match.data(match.it)
#Standard data manipulation
df5 <- select(df4,LATEST_CUST_SCV_ID)
df6 <- df %>%
  filter(POST_PERIOD==1)
#Get Post period results
df7 <- merge(x = df5, y = df6, by="LATEST_CUST_SCV_ID", all.x = TRUE)
#Some summary stats for Post period
df7 %>%
  group_by(TARGET) %>%
  summarise(mean_S=mean(TOTAL_SALES),sum_S=sum(TOTAL_SALES),mean_TRANS=mean(TOTAL_TRANS),sum_TRANS=sum(TOTAL_TRANS))
##########################################################
##########################################################
#Get pre period results
df8 <- merge(x = df5, y = df1, by="LATEST_CUST_SCV_ID", all.x = TRUE)
#Some summary stats for Pre period
df8 %>%
  group_by(TARGET) %>%
  summarise(mean_TS=mean(TOTAL_SALES),sum_TS=sum(TOTAL_SALES),mean_TTrans=mean(TOTAL_TRANS),sum_TTrans=sum(TOTAL_TRANS))
count(df7,TARGET)
#T Test to compare pre period
res <- t.test(TOTAL_SALES ~ TARGET, data = df8)
res
#T Test to compare post period
res <- t.test(TOTAL_SALES ~ TARGET, data = df7)
res



bigrquery::bq_table_upload("aw-insight-dev-live-20.zscratch_emiben.SI_IPICKCTRL_OUT1",df8,write_disposition = "WRITE_TRUNCATE")